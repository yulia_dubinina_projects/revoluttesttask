package testtask.service;

import testtask.model.Account;
import testtask.model.TransferResult;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeMap;

public class AccountService {

    private TreeMap<Long, Account> accounts = new TreeMap<>();

    private HashSet<Long> locked = new HashSet<>();

    private synchronized void lockId(long accountId) {
        while (locked.contains(accountId)) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        locked.add(accountId);
    }

    private synchronized void unlockId(long accountId) {
        locked.remove(accountId);
        notifyAll();
    }

    private static AccountService instance = null;

    public static synchronized AccountService getInstance() {
        if (instance == null) {
            instance = new AccountService();
        }
        return instance;
    }

    private AccountService() {
    }

    public Account addAccount(Account account) {
        lockId(account.getAccountId());
        if (accounts.containsKey(account.getAccountId())) {
            Account res = accounts.get(account.getAccountId());
            res = copyAccount(res);
            unlockId(account.getAccountId());
            return res;
        }
        accounts.put(account.getAccountId(), account);
        unlockId(account.getAccountId());
        return null;
    }

    private Account copyAccount(Account res) {
        res = new Account(res.getAccountId(), res.getBalance());
        return res;
    }

    public Account deleteAccount(long accountId) {
        lockId(accountId);
        Account res = accounts.remove(accountId);
        if (res != null) {
            res = copyAccount(res);
        }
        unlockId(accountId);
        return res;
    }


    public Collection<Account> getAllAccounts() {
        return accounts.values();
    }

    public Account getAccount(long accountId) {
        lockId(accountId);
        Account res = accounts.get(accountId);
        if (res != null) {
            res = copyAccount(res);
        }
        unlockId(accountId);
        return res;
    }

    public synchronized TransferResult transfer(long from, long to, double amount) {
        if (from == to) {
            return null;
        }
        long first;
        long second;
        if (from < to) {
            first = from;
            second = to;
        } else {
            first = to;
            second = from;
        }
        lockId(first);
        lockId(second);
        Account accountFrom = accounts.get(from);
        Account accountTo = accounts.get(to);
        boolean fromNotFound = accountFrom == null;
        boolean toNotFound = accountTo == null;
        if (fromNotFound || toNotFound) {
            TransferResult res = new TransferResult(fromNotFound, toNotFound, false, BigDecimal.valueOf(0), BigDecimal.valueOf(0));
            unlockId(second);
            unlockId(first);
            return res;
        }
        BigDecimal amountBig = BigDecimal.valueOf(amount);
        if (accountFrom.getBalance().compareTo(amountBig) < 0) {
            TransferResult res = new TransferResult(false, false, true, accountFrom.getBalance(), accountTo.getBalance());
            unlockId(second);
            unlockId(first);
            return res;
        }
        accountFrom.setBalance(accountFrom.getBalance().subtract(amountBig));
        accountTo.setBalance(accountTo.getBalance().add(amountBig));
        TransferResult res = new TransferResult(false, false, false, accountFrom.getBalance(), accountTo.getBalance());
        unlockId(second);
        unlockId(first);
        return res;
    }
}
