package testtask.model;

public class SuccessOrErrorMessage {


    private boolean success = false;

    private String message;

    public SuccessOrErrorMessage() {
    }

    public SuccessOrErrorMessage(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}
