package testtask.model;

import java.math.BigDecimal;

public class Account {

    private long accountId;
    private BigDecimal balance;

    public Account() {
    }

    public Account(long accountId, BigDecimal balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

    public long getAccountId() {
        return accountId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "[id=" + accountId + " balance=" + balance + "]";
    }
}
