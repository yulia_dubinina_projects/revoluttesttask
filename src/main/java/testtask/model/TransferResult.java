package testtask.model;

import java.math.BigDecimal;

public class TransferResult {

    private boolean fromNotFound;
    private boolean toNotFound;
    private boolean lowBalance;
    private BigDecimal fromResult;
    private BigDecimal toResult;

    public TransferResult(boolean fromNotFound, boolean toNotFound, boolean lowBalance, BigDecimal fromResult, BigDecimal toResult) {
        this.fromNotFound = fromNotFound;
        this.toNotFound = toNotFound;
        this.lowBalance = lowBalance;
        this.toResult = toResult;
        this.fromResult = fromResult;
    }

    public boolean isFromNotFound() {
        return fromNotFound;
    }

    public boolean isToNotFound() {
        return toNotFound;
    }

    public boolean isLowBalance() {
        return lowBalance;
    }

    public BigDecimal getToResult() {
        return toResult;
    }

    public BigDecimal getFromResult() {
        return fromResult;
    }
}
