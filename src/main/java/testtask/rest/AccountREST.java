package testtask.rest;

import testtask.model.Account;
import testtask.model.SuccessOrErrorMessage;
import testtask.model.TransferResult;
import testtask.service.AccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("account")
public class AccountREST {

    private AccountService accountService = AccountService.getInstance();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAccount(
            @FormParam("accountId") long accountId,
            @FormParam("balance") double balance) {
        Account newAccount = new Account(accountId, BigDecimal.valueOf(balance));
        Account existing = accountService.addAccount(newAccount);
        if (existing == null) {
            SuccessOrErrorMessage result = new SuccessOrErrorMessage(true, "Account saved: " + newAccount);
            return Response.status(201).entity(result).build();
        }
        SuccessOrErrorMessage result = new SuccessOrErrorMessage(false, "Account already exists: " + existing);
        return Response.status(409).entity(result).build();

    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccount(
            @FormParam("accountId") long accountId) {
        Account deleted = accountService.deleteAccount(accountId);
        if (deleted == null) {
            SuccessOrErrorMessage result = new SuccessOrErrorMessage(false, "Account with id=" + accountId + " does not exist");
            return Response.status(404).entity(result).build();
        }
        SuccessOrErrorMessage result = new SuccessOrErrorMessage(true, "Account deleted: " + deleted);
        return Response.status(200).entity(result).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(
            @QueryParam("accountId") long accountId) {
        Account account = accountService.getAccount(accountId);
        if (account == null) {
            SuccessOrErrorMessage result = new SuccessOrErrorMessage(false, "Account with id=" + accountId + " does not exist");
            return Response.status(404).entity(result).build();
        }
        return Response.status(200).entity(account).build();

    }


    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfer(
            @FormParam("from") long from,
            @FormParam("to") long to,
            @FormParam("amount") double amount) {
        TransferResult res = accountService.transfer(from, to, amount);
        if (res == null) {
            SuccessOrErrorMessage result = new SuccessOrErrorMessage(false, "Account ids are equal (" + from + ")");
            return Response.status(405).entity(result).build();
        }
        if (res.isFromNotFound() || res.isToNotFound()) {
            String message = "";
            if (res.isFromNotFound()) {
                message = "Account with id=" + from + " from which to transfer does not exist";
            }
            if (res.isToNotFound()) {
                if (!message.isEmpty()) {
                    message += "\n";
                }
                message += "Account with id=" + to + " to which to transfer does not exist";
            }
            SuccessOrErrorMessage result = new SuccessOrErrorMessage(false, message);
            return Response.status(404).entity(result).build();
        }
        Account accountFrom = new Account(from, res.getFromResult());
        Account accountTo = new Account(to, res.getToResult());
        if (res.isLowBalance()) {
            SuccessOrErrorMessage result = new SuccessOrErrorMessage(false, "Cannot transfer from " + accountFrom + " to " + accountTo + ". Insufficient funds");
            return Response.status(405).entity(result).build();
        }
        SuccessOrErrorMessage result = new SuccessOrErrorMessage(true, "Transfer completed. New balances: from=" + accountFrom + " to=" + accountTo);
        return Response.status(200).entity(result).build();

    }
}

