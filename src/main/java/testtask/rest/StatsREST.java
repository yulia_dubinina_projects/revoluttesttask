package testtask.rest;

import testtask.model.Account;
import testtask.service.AccountService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("stats")
public class StatsREST {

    private AccountService accountService = AccountService.getInstance();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Account[] addAllAccounts() {
        return accountService.getAllAccounts().toArray(new Account[0]);
    }
}

