package testtask;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.WebComponent;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {

        Logger jerseyLogger = Logger.getLogger(WebComponent.class.getName());
        jerseyLogger.setFilter(record -> {
            boolean isLoggable = true;
            if (record.getMessage().contains("Only resource methods using @FormParam")) {
                isLoggable = false;
            }
            return isLoggable;
        });

        Server server = new Server(8080);

        ServletContextHandler ctx =
                new ServletContextHandler(ServletContextHandler.NO_SESSIONS);

        ctx.setContextPath("/");
        server.setHandler(ctx);

        ServletHolder serHol = ctx.addServlet(ServletContainer.class, "/rest/*");
        serHol.setInitOrder(1);
        serHol.setInitParameter("jersey.config.server.provider.packages",
                "testtask.rest");

        Thread t = new Thread(() -> {
            try {
                server.start();
                server.join();
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                server.destroy();
            }
        });
        t.start();
    }
}