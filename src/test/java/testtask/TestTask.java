package testtask;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.filter.log.RequestLoggingFilter;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import testtask.model.Account;
import testtask.model.SuccessOrErrorMessage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class TestTask {
    private static RequestSpecification spec;
    private static RequestSpecification specNoFilters;
    private static RequestSpecification specWithFilters;

    @BeforeClass
    public static void initSpec() {
        Main.main(new String[0]);
        specWithFilters = new RequestSpecBuilder()
                .setContentType(ContentType.URLENC)
                .setBaseUri("http://localhost:8080/")
                .addFilter(new ResponseLoggingFilter())
                .addFilter(new RequestLoggingFilter())
                .build();
        specNoFilters = new RequestSpecBuilder()
                .setContentType(ContentType.URLENC)
                .setBaseUri("http://localhost:8080/")
                .build();
        spec = specWithFilters;
    }

    @Test
    public void deadLockTest() {
        spec = specNoFilters;
        addAccountWithSuccess(1, 100000);
        addAccountWithSuccess(2, 100000);
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        List<Future<Boolean>> res=new ArrayList<>();
        for (int i = 0; i < 1000; i++) {

            Future<Boolean> futureAB = executorService.submit(() -> {
                executeTransfer(1, 2, 1, 200);
                return true;
            });
            Future<Boolean> futureBA = executorService.submit(() -> {
                executeTransfer(2, 1, 1, 200);
                return true;
            });
            res.add(futureAB);
            res.add(futureBA);
        }
        for(Future<Boolean> future:res){
            try {
                Assert.assertTrue(future.get());
            } catch (InterruptedException | ExecutionException e) {
                Assert.fail(e.getMessage());
            }
        }
        executorService.shutdown();
        //clean up
        deleteAccountWithSuccess(1, 100000);
        deleteAccountWithSuccess(2, 100000);
        spec = specWithFilters;
    }

    @Test
    public void transfer() {
        double fromBalance = 100;
        double toBalance = 50;
        double amount = 20;
        addAccountWithSuccess(1, fromBalance);
        addAccountWithSuccess(2, toBalance);
        executeTransfer(1, 1, amount, 405, "Account ids are equal (1)");
        executeTransfer(3, 1, amount, 404,
                "Account with id=3 from which to transfer does not exist");
        executeTransfer(1, 3, amount, 404,
                "Account with id=3 to which to transfer does not exist");
        executeTransfer(3, 4, amount, 404,
                "Account with id=3 from which to transfer does not exist\n" +
                        "Account with id=4 to which to transfer does not exist");
        executeTransfer(1, 2, fromBalance + amount, 405,
                "Cannot transfer from [id=1 balance=" + fromBalance + "] to [id=2 balance=" + toBalance + "]. Insufficient funds");
        fromBalance -= amount;
        toBalance += amount;
        executeTransfer(1, 2, 20, 200,
                "Transfer completed. New balances: from=[id=1 balance=" + fromBalance + "] to=[id=2 balance=" + toBalance + "]");
        //clean up
        deleteAccountWithSuccess(1, 80);
        deleteAccountWithSuccess(2, 70);
    }

    private void executeTransfer(long from, long to, double amount, int code, String response) {
        SuccessOrErrorMessage res = executeTransfer(from, to, amount, code);
        Assert.assertEquals(response, res.getMessage());
    }

    private SuccessOrErrorMessage executeTransfer(long from, long to, double amount, int code) {
        SuccessOrErrorMessage res = RestAssured
                .given()
                .spec(spec)
                .param("from", from)
                .param("to", to)
                .param("amount", amount)
                .when()
                .put("rest/account")
                .then()
                .statusCode(code)
                .extract().as(SuccessOrErrorMessage.class);
        Assert.assertEquals(code == 200, res.isSuccess());
        return res;
    }

    @Test
    public void getAllAccounts() {
        addAccountWithSuccess(5, 3);
        addAccountWithSuccess(3, 1);
        addAccountWithSuccess(7, 5);
        addAccountWithSuccess(6, 4);
        addAccountWithSuccess(4, 2);
        Account[] res = RestAssured
                .given()
                .spec(spec)
                .when()
                .get("rest/stats")
                .then()
                .statusCode(200)
                .extract().as(Account[].class);
        Assert.assertEquals(res.length, 5);
        aassertAccount(res[0], 3, 1);
        aassertAccount(res[1], 4, 2);
        aassertAccount(res[2], 5, 3);
        aassertAccount(res[3], 6, 4);
        aassertAccount(res[4], 7, 5);
        //clean up
        deleteAccountWithSuccess(3, 1);
        deleteAccountWithSuccess(4, 2);
        deleteAccountWithSuccess(5, 3);
        deleteAccountWithSuccess(6, 4);
        deleteAccountWithSuccess(7, 5);
    }

    @Test
    public void getAccount() {
        addAccountWithSuccess(1, 2);
        Account resAcc = RestAssured
                .given()
                .spec(spec)
                .queryParam("accountId", 1)
                .when()
                .get("rest/account")
                .then()
                .statusCode(200)
                .extract().as(Account.class);
        aassertAccount(resAcc, 1, 2);

        SuccessOrErrorMessage resMsg = RestAssured
                .given()
                .spec(spec)
                .queryParam("accountId", 3)
                .when()
                .get("rest/account")
                .then()
                .statusCode(404)
                .extract().as(SuccessOrErrorMessage.class);
        Assert.assertFalse(resMsg.isSuccess());
        Assert.assertEquals("Account with id=" + 3 + " does not exist", resMsg.getMessage());

        //clean up
        deleteAccountWithSuccess(1, 2);
    }

    private void aassertAccount(Account account, long accountId, double balance) {
        Assert.assertEquals(account.getAccountId(), accountId);
        Assert.assertEquals(account.getBalance(), BigDecimal.valueOf(balance));
    }

    @Test
    public void addAndDeleteAccountsTest() {
        addAccountWithSuccess(1, 1);
        addAccountWithFailure(1, 2, 1);
        deleteAccountWithFailure(2);
        deleteAccountWithSuccess(1, 1);
        addAccountWithSuccess(1, 1);
        //clean up
        deleteAccountWithSuccess(1, 1);
    }

    private void addAccountWithSuccess(long accountId, double balance) {
        SuccessOrErrorMessage res = addAccount(accountId, balance, 201);
        Assert.assertTrue(res.isSuccess());
        Assert.assertEquals("Account saved: [id=" + accountId + " balance=" + balance + "]", res.getMessage());
    }

    private void addAccountWithFailure(long accountId, double balance, double existingBalance) {
        SuccessOrErrorMessage res = addAccount(accountId, balance, 409);
        Assert.assertFalse(res.isSuccess());
        Assert.assertEquals("Account already exists: [id=" + accountId + " balance=" + existingBalance + "]", res.getMessage());
    }

    private SuccessOrErrorMessage addAccount(long accountId, double balance, int code) {
        return RestAssured
                .given()
                .spec(spec)
                .param("accountId", accountId)
                .param("balance", balance)
                .when()
                .post("rest/account")
                .then()
                .statusCode(code)
                .extract().as(SuccessOrErrorMessage.class);
    }

    private void deleteAccountWithSuccess(long accountId, double existingBalance) {
        SuccessOrErrorMessage res = deleteAccount(accountId, 200);
        Assert.assertTrue(res.isSuccess());
        Assert.assertEquals("Account deleted: [id=" + accountId + " balance=" + existingBalance + "]", res.getMessage());
    }

    private void deleteAccountWithFailure(long accountId) {
        SuccessOrErrorMessage res = deleteAccount(accountId, 404);
        Assert.assertFalse(res.isSuccess());
        Assert.assertEquals("Account with id=" + accountId + " does not exist", res.getMessage());
    }

    private SuccessOrErrorMessage deleteAccount(long accountId, int code) {
        return RestAssured
                .given()
                .spec(spec)
                .param("accountId", accountId)
                .when()
                .delete("rest/account")
                .then()
                .statusCode(code)
                .extract().as(SuccessOrErrorMessage.class);
    }
}
