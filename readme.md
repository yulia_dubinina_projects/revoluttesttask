#Java/Scala Test

Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts.

##Explicit requirements:

1. keep it simple and to the point (e.g. no need to implement any authentication, assume the APi is invoked by another internal system/service)
2. use whatever frameworks/libraries you like (except Spring, sorry!) but don't forget about the requirement #1
3. the datastore should run in-memory for the sake of this test
4. the final result should be executable as a standalone program (should not require a pre-installed container/server)
5. demonstrate with tests that the API works as expected

##Implicit requirements:

1. the code produced by you is expected to be of high quality.
2. there are no detailed requirements, use common sense.

Please put your work on github or bitbucket.

#API description

###**Show all accounts**
**URL:** /rest/stats  
**Method:** GET  
**URL Params:** none  
**Data Params:** none  
**Success Response example:**  
Code: 200  
Content:
```
[{"accountId":3,"balance":1.0},{"accountId":4,"balance":2.0},{"accountId":5,"balance":3.0},{"accountId":6,"balance":4.0},{"accountId":7,"balance":5.0}]
```

###**Get account info**
**URL:** /rest/account?accountId=:id  
**Method:** GET  
**URL Params:** accountId=[long]  
**Data Params:** none  
**Success Response example:**  
Code: 200  
Content:
```
{"accountId":1,"balance":2.0}
```
**Error Response example:**  
Code: 404  
Content:
```
{"success":false,"message":"Account with id=3 does not exist"}
```

###**Add account**
**URL:** /rest/account?accountId=:id&balance=:balance  
**Method:** POST  
**URL Params:** none  
**Data Params:** accountId=[long] balance=[double]  
**Success Response example:**  
Code: 200  
Content:
```
{"success":true,"message":"Account saved: [id=1 balance=1.0]"}
```
**Error Response example:**  
Code: 409  
Content:  
```
{"success":false,"message":"Account already exists: [id=1 balance=1.0]"}
```

###**Delete account**
**URL:** /rest/account?accountId=:id  
**Method:** DELETE  
**URL Params:** none  
**Data Params:** accountId=[long]  
**Success Response example:**  
Code: 200  
Content:
```
{"success":true,"message":"Account deleted: [id=1 balance=1.0]}"
```
**Error Response example:**  
Code: 404  
Content:
```
{"success":false,"message":"Account with id=2 does not exist"}
```

###**Transfer**
**URL:** /rest/account?from==:idFrom&to=idTo&amount=:amount  
**Method:** PUT  
**URL Params:** none  
**Data Params:** from=[long] to=[long] amount=[double]  
**Success Response example:**  
Code: 200  
Content:
```
{"success":true,"message":"Transfer completed. New balances: from=[id=1 balance=80.0] to=[id=2 balance=70.0]"}
```
**Error Response example:**  
Code: 405  
Content:
```
{"success":false,"message":"Account ids are equal (1)"}
```
Code: 404  
Content:
```
{"success":false,"message":"Account with id=3 from which to transfer does not exist"}
```
Code: 404  
Content:
```
{"success":false,"message":"Account with id=3 to which to transfer does not exist"}
```
Code: 404  
Content:
```
{"success":false,"message":"Account with id=3 from which to transfer does not exist\nAccount with id=4 to which to transfer does not exist"}

```
Code: 405  
Content:
```
{"success":false,"message":"Cannot transfer from [id=1 balance=100.0] to [id=2 balance=50.0]. Insufficient funds"}
```
